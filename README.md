# solidsnake
A simple (and small) wrapper around IronPython functions for C# designed for ease of use and reduction of IPython boilerplate.

# Build and use
1. Download the code as .zip or pull from GitHub
2. Open the "solidsnake" Visual Studio solution file (only tested with VS 2015 but should work with 2010+)
3. Make sure your Startup Project is on "solidsnake" and that your build config is on Release.
4. Hit Build -> Build solidsnake
5. Add the .dll as a reference in your VS project/solution and include "using solidsnake;" at the top of your code

# Authors
Anthony "Anthony-y" Baynham
