﻿using System;

namespace solidsnake
{
    public class Script
    {
        public string Path { get; set; }
        public string Text { get; set; }

        public Script(string path, string text = "")
        {
            Path = path;
            Text = "";
        }
    }
}
