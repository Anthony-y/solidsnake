﻿using System;

using IronPython.Hosting;
using IronPython.Runtime;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;

namespace solidsnake
{
    public class ScriptManager
    {
        public ScriptEngine pyEngine { get; set; } = null;
        public ScriptScope pyScope   { get; set; } = null;

        public Script Script { get; set; }

        private dynamic pyClass;

        public ScriptManager(Script script)
        {
            if (pyEngine == null && script != null)
            {
                Script = script;
                pyEngine = Python.CreateEngine();
                pyScope = pyEngine.CreateScope();
            }
        }

        public void CompileAndRunFromText()
        {
            ScriptSource source = pyEngine.CreateScriptSourceFromString
                        (Script.Text, SourceCodeKind.Statements);
            CompiledCode compiled = source.Compile();
            compiled.Execute(pyScope);
        }

        public void CompileAndRunFromText(string code)
        {
            ScriptSource source = pyEngine.CreateScriptSourceFromString
                        (code, SourceCodeKind.Statements);
            CompiledCode compiled = source.Compile();
            compiled.Execute(pyScope);
        }

        public void CompileAndRunFromPath()
        {
            ScriptSource source = pyEngine.CreateScriptSourceFromFile
                        (Script.Path, System.Text.Encoding.Default);
            CompiledCode compiled = source.Compile();
            compiled.Execute(pyScope);
        }
        
        public void CallMethod(string funcName, params dynamic[] arguments)
        {
            pyEngine.Operations.InvokeMember(pyClass, funcName, arguments);
        }

        public dynamic RegisterClass(string className)
        {
            pyClass = pyEngine.Operations.Invoke(pyScope.GetVariable(className));
            return pyClass;
        }

        public void RegPyVar(string name, object obj) { pyScope.SetVariable(name, obj); }
    }
}
