﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using solidsnake;

namespace Tests
{
    public class TestClass
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            ScriptManager scrMan = new ScriptManager(new Script("test.py"));
            TestClass testClass = new TestClass() { Age = 15, Name = "Anthony" };
            scrMan.RegPyVar("testClass", testClass);
            scrMan.RegPyVar("txtBox", txt_Box);
            scrMan.CompileAndRunFromPath();
            scrMan.CallMethod("abc", "_123");
            scrMan.CompileAndRunFromText("testFunc()");
        }


    }
}
