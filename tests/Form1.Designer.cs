﻿namespace Tests
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_ClickHere = new System.Windows.Forms.Button();
            this.txt_Box = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bt_ClickHere
            // 
            this.bt_ClickHere.Location = new System.Drawing.Point(13, 13);
            this.bt_ClickHere.Name = "bt_ClickHere";
            this.bt_ClickHere.Size = new System.Drawing.Size(352, 36);
            this.bt_ClickHere.TabIndex = 0;
            this.bt_ClickHere.Text = "Click here";
            this.bt_ClickHere.UseVisualStyleBackColor = true;
            // 
            // txt_Box
            // 
            this.txt_Box.Location = new System.Drawing.Point(13, 56);
            this.txt_Box.Multiline = true;
            this.txt_Box.Name = "txt_Box";
            this.txt_Box.Size = new System.Drawing.Size(352, 193);
            this.txt_Box.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 261);
            this.Controls.Add(this.txt_Box);
            this.Controls.Add(this.bt_ClickHere);
            this.Name = "Form1";
            this.Text = "Test";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_ClickHere;
        private System.Windows.Forms.TextBox txt_Box;
    }
}

